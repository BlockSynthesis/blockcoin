## BlockCoin Alpha Block-Code
=====================================

[![Build Status](https://travis-ci.org/bitcoin/bitcoin.svg?branch=master)](https://travis-ci.org/bitcoin/bitcoin)

https://bitcoincore.org

## What is BlockCoin?
----------------

BlockCoin is an digital currency that enables instant payments to
anyone, anywhere in the world. BlockCoin uses peer-to-peer technology to operate
with no central authority: managing transactions and issuing money are carried
out collectively by the network. BlockCoin Alpha Block-Code is the name of open source
software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of
the Alpha Block-Code software, see https://alphablockcode.org/en/download, or read the
[original whitepaper](https://alphablockcode.org/BlockCoin.pdf).
